* With more time the Price Publisher class should be refactored so the repricing happens in its own class.
* Apache commons math was used for the cumulative distribution.
* Only stock information is stored (initial price, expected return and the standard deviation). Options are priced from this static data. 
* BigDecimals are used for the storage and transport of financial numbers, doubles are returned from the Black Scholes and the Brownian function. This is acceptable as both are estimates.
* It is assumed that the option expires on the 1st of the month provided.
* The output format could be improved, and has some size limitations already, but to suggest a better solution requires a better understanding of how the data will be used.
* The existing input format has been retained.
* Improvement is possible with all the thread handling, particularly in the event of a recovery being neccessary.

You can run this project with the following command:
./gradlew bootJar && chmod a+x build/libs/crypto-homework-0.0.1-SNAPSHOT.jar && build/libs/./crypto-homework-0.0.1-SNAPSHOT.jar < example-input.csv