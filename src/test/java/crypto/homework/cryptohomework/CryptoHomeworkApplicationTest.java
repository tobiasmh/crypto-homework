package crypto.homework.cryptohomework;

import crypto.homework.cryptohomework.model.valuation.PortfolioValuationResult;
import crypto.homework.cryptohomework.publisher.PricePublisher;
import crypto.homework.cryptohomework.subscriber.PortfolioPrinter;
import crypto.homework.cryptohomework.subscriber.PositionInputReader;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import static org.mockito.Mockito.*;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class CryptoHomeworkApplicationTest {

    final private PricePublisher pricePublisher = mock(PricePublisher.class);
    final private PortfolioPrinter portfolioPrinter = mock(PortfolioPrinter.class);
    final private PositionInputReader positionInputReader = mock(PositionInputReader.class);

    final private CryptoHomeworkApplication cryptoHomeworkApplication = new CryptoHomeworkApplication(pricePublisher,
            portfolioPrinter,
            positionInputReader);

    @Test
    public void Positions_Are_Read_And_Publisher_Is_Subscribed_To() throws Exception {

        Thread thread = startApplicationThread();
        thread.start();

        verify(positionInputReader, timeout(1000)).readPositions();
        verify(pricePublisher, timeout(1000)).subscribe(any());

        thread.interrupt();

    }

    @Test
    public void Publisher_Callback_Invokes_Printer() {
        Thread thread = startApplicationThread();
        thread.start();

        ArgumentCaptor<PricePublisher.PriceUpdateCallback> argumentCaptor = ArgumentCaptor.forClass(PricePublisher.PriceUpdateCallback.class);
        verify(pricePublisher, timeout(1000)).subscribe(argumentCaptor.capture());

        PortfolioValuationResult mockPortfolioValuationResult = mock(PortfolioValuationResult.class);
        argumentCaptor.getValue().priceIsUpdated(mockPortfolioValuationResult);
        verify(portfolioPrinter, timeout(1000)).printValuation(eq(mockPortfolioValuationResult));

        thread.interrupt();
    }

    private Thread startApplicationThread() {
        Runnable runnable = () -> {
            try {
                cryptoHomeworkApplication.run();
            } catch (Exception e) {
                e.printStackTrace();
            }
        };
        return new Thread(runnable);

    }

}