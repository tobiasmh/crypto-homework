package crypto.homework.cryptohomework.publisher;

import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.MathContext;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class BlackScholesPricerTest {

    private final BlackScholesPricer pricer = new BlackScholesPricer();

    @Test
    public void Call_Price_Should_Have_Parity_With_Put_Price() {
        double callPrice = pricer.callOption(60, 58, 0.035, 0.5, 0.2);
        double callParity = callPrice + 58 * Math.exp(-(0.035 * 0.5));

        double putPrice = pricer.putOption(60, 58, 0.035, 0.5, 0.2);
        double putParity = putPrice + 60 * Math.exp(-(0 * 0.5));

        BigDecimal putParityDecimalValue = BigDecimal.valueOf(putParity).round(MathContext.DECIMAL32);
        BigDecimal callParityDecimalValue = BigDecimal.valueOf(callParity).round(MathContext.DECIMAL32);

        assertThat(putParityDecimalValue.equals(callParityDecimalValue)).isTrue();
    }


}