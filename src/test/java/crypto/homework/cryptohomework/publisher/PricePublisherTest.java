package crypto.homework.cryptohomework.publisher;

import crypto.homework.cryptohomework.StockRepository;
import crypto.homework.cryptohomework.model.instrument.OptionType;
import crypto.homework.cryptohomework.model.instrument.Stock;
import crypto.homework.cryptohomework.model.position.OptionPosition;
import crypto.homework.cryptohomework.model.position.PortfolioPosition;
import crypto.homework.cryptohomework.model.position.StockPosition;
import crypto.homework.cryptohomework.model.valuation.PortfolioValuationResult;
import crypto.homework.cryptohomework.model.valuation.PositionValuation;
import crypto.homework.cryptohomework.util.DateUtil;
import org.junit.jupiter.api.*;
import org.mockito.ArgumentCaptor;

import java.math.BigDecimal;
import java.net.ConnectException;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.awaitility.Awaitility.await;
import static org.mockito.Mockito.*;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class PricePublisherTest {

    private final BrownianMotionDiscreteTimeGenerator brownianGenerator = mock(BrownianMotionDiscreteTimeGenerator.class);

    private final StockRepository stockRepository = mock(StockRepository.class);
    private final Random random = mock(Random.class);

    private final Clock clock = Clock.fixed(
            Instant.parse("2021-05-23T10:00:00Z"),
            ZoneOffset.UTC);

    private final BlackScholesPricer blackScholesPricer = mock(BlackScholesPricer.class);

    private PricePublisher pricePublisher;

    private List<PortfolioPosition> portfolioPositions;

    @BeforeEach
    public void before() {
        Stock exampleStock = new Stock("EXAMPLE",
                new BigDecimal("100"),
                new BigDecimal("0.25"),
                new BigDecimal("0.10")
        );
        when(stockRepository.findAll()).thenReturn(Collections.singletonList(exampleStock));
        when(random.nextBoolean()).thenReturn(true);

        portfolioPositions = new LinkedList<>();
        portfolioPositions.add(new StockPosition("EXAMPLE", new BigDecimal("500")));

        pricePublisher = new PricePublisher(stockRepository, brownianGenerator, blackScholesPricer, clock, random);
    }

    @AfterEach
    public void after() {
        pricePublisher.interruptPricePublisherThread();
    }

    @Test
    public void Stock_Price_Is_Updated_With_Random_Brownian_Value() {

        when(brownianGenerator.generateReturn(anyDouble(), anyDouble(), anyDouble())).thenReturn(0.01);

        await().atMost(5, TimeUnit.SECONDS).ignoreException(ConnectException.class).until(() -> {
            ArgumentCaptor<Stock> argumentCaptor = ArgumentCaptor.forClass(Stock.class);
            verify(stockRepository).save(argumentCaptor.capture());
            Stock stock = argumentCaptor.getValue();
            assertThat(stock.getPrice()).isEqualTo(new BigDecimal("101.0"));
            return true;
        });

    }

    @Test
    public void Stock_Price_Is_Unchanged_The_Position_Valuation_Is_Unchanged() {
        reset(random);
        when(random.nextBoolean()).thenReturn(false);

        List<PortfolioValuationResult> portfolioValuationResult = new LinkedList<>();

        subscribeToPriceUpdates(portfolioValuationResult, portfolioPositions);

        await().atMost(5, TimeUnit.SECONDS).ignoreException(IndexOutOfBoundsException.class).until(() -> {
            PortfolioValuationResult valuationResult = portfolioValuationResult.get(0);
            assertThat(valuationResult.getPositionValuations().get(0).getUpdated()).isFalse();
            return true;
        });

    }

    @Test
    public void Callback_Contains_Stock_Position_Then_Position_Is_Updated() {

        List<PortfolioValuationResult> portfolioValuationResult = new LinkedList<>();

        subscribeToPriceUpdates(portfolioValuationResult, portfolioPositions);

        await().atMost(5, TimeUnit.SECONDS).ignoreException(IndexOutOfBoundsException.class).until(() -> {
            PortfolioValuationResult valuationResult = portfolioValuationResult.get(0);
            PositionValuation positionValuation = valuationResult.getPositionValuations().get(0);
            assertThat(positionValuation.getUpdated()).isTrue();
            assertThat(positionValuation.getPosition()).isEqualTo(new BigDecimal("500"));
            assertThat(positionValuation.getPositionValue()).isEqualTo(new BigDecimal("50000.0"));
            return true;
        });

    }

    @Test
    public void Callback_Contains_Call_Option_Position_Then_Position_Is_Updated() {

        List<PortfolioValuationResult> portfolioValuationResult = new LinkedList<>();

        List<PortfolioPosition> portfolioPositions = new LinkedList<>();
        portfolioPositions.add(new OptionPosition("EXAMPLE", new BigDecimal("200"),
                new BigDecimal("120"),
                LocalDate.now(),
                OptionType.CALL));

        when(blackScholesPricer.callOption(anyDouble(), anyDouble(), anyDouble(), anyDouble(), anyDouble()))
                .thenReturn(225.0);

        subscribeToPriceUpdates(portfolioValuationResult, portfolioPositions);

        await().atMost(5, TimeUnit.SECONDS).ignoreException(IndexOutOfBoundsException.class).until(() -> {
            PortfolioValuationResult valuationResult = portfolioValuationResult.get(0);
            PositionValuation positionValuation = valuationResult.getPositionValuations().get(0);
            assertThat(positionValuation.getUpdated()).isTrue();
            assertThat(positionValuation.getPosition()).isEqualTo(new BigDecimal("200"));
            assertThat(positionValuation.getPrice()).isEqualTo(new BigDecimal("225"));
            assertThat(positionValuation.getPositionValue()).isEqualTo(new BigDecimal("45000"));
            return true;
        });
    }

    @Test
    public void Callback_Contains_Put_Option_Position_Then_Position_Is_Updated() {
        List<PortfolioValuationResult> portfolioValuationResult = new LinkedList<>();

        List<PortfolioPosition> portfolioPositions = new LinkedList<>();
        portfolioPositions.add(new OptionPosition("EXAMPLE", new BigDecimal("-2000"),
                new BigDecimal("250"),
                LocalDate.now(),
                OptionType.PUT));

        when(blackScholesPricer.putOption(anyDouble(), anyDouble(), anyDouble(), anyDouble(), anyDouble()))
                .thenReturn(65.0);

        subscribeToPriceUpdates(portfolioValuationResult, portfolioPositions);

        await().atMost(5, TimeUnit.SECONDS).ignoreException(IndexOutOfBoundsException.class).until(() -> {
            PortfolioValuationResult valuationResult = portfolioValuationResult.get(0);
            PositionValuation positionValuation = valuationResult.getPositionValuations().get(0);
            assertThat(positionValuation.getUpdated()).isTrue();
            assertThat(positionValuation.getPosition()).isEqualTo(new BigDecimal("-2000"));
            assertThat(positionValuation.getPrice()).isEqualTo(new BigDecimal("65"));
            assertThat(positionValuation.getPositionValue()).isEqualTo(new BigDecimal("-130000"));
            return true;
        });
    }

    @Test
    public void Time_Until_Option_Expiry_Is_Calculated_Correctly() {

        List<PortfolioPosition> portfolioPositions = new LinkedList<>();
        portfolioPositions.add(new OptionPosition("EXAMPLE", new BigDecimal("200"),
                new BigDecimal("120"),
                LocalDate.parse("OCT-2025", DateUtil.dateFormatter),
                OptionType.CALL));

        subscribeToPriceUpdates(new LinkedList<>(), portfolioPositions);

        ArgumentCaptor<Double> argumentCaptor = ArgumentCaptor.forClass(Double.class);
        verify(blackScholesPricer, timeout(5000)).callOption(anyDouble(), anyDouble(), anyDouble(), argumentCaptor.capture(), anyDouble());

        assertThat(argumentCaptor.getValue()).isEqualTo(4.358747959136963);
    }

    private void subscribeToPriceUpdates(List<PortfolioValuationResult> portfolioValuationResult,
                                         List<PortfolioPosition> portfolioPositions) {
        pricePublisher.subscribe(new PricePublisher.PriceUpdateCallback() {
            @Override
            public void priceIsUpdated(PortfolioValuationResult updatedPortfolioValuationResult) {
                portfolioValuationResult.add(updatedPortfolioValuationResult);
            }

            @Override
            public List<PortfolioPosition> portfolioPositions() {
                return portfolioPositions;
            }
        });
    }

}