package crypto.homework.cryptohomework.model.valuation;

import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class PortfolioValuationResultTest {

    @Test
    public void Valuation_Net_Asset_Value_Is_Calculated_Correctly() {

        List<PositionValuation> positionValuation = Arrays.asList(new PositionValuation("AAPL"
                        , new BigDecimal("100")
                        , new BigDecimal("500")
                        , new BigDecimal("50000"),
                        true
                ),
                new PositionValuation("TESLA"
                        , new BigDecimal("150")
                        , new BigDecimal("600")
                        , new BigDecimal("90000"),
                        true
                ));
        PortfolioValuationResult valuationResult = new PortfolioValuationResult(positionValuation);

        assertThat(valuationResult.getNetAssetValue()).isEqualTo(new BigDecimal("140000"));
    }

}