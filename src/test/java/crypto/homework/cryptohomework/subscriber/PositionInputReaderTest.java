package crypto.homework.cryptohomework.subscriber;

import crypto.homework.cryptohomework.model.instrument.OptionType;
import crypto.homework.cryptohomework.model.position.OptionPosition;
import crypto.homework.cryptohomework.model.position.PortfolioPosition;
import crypto.homework.cryptohomework.util.DateUtil;
import org.junit.jupiter.api.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class PositionInputReaderTest {

    private final InputStream originalIn = System.in;

    private final PositionInputReader positionInputReader = new PositionInputReader();

    @AfterEach
    public void after() {
        System.setIn(originalIn);
    }

    private void assertOptionPosition(PortfolioPosition portfolioPosition, Long position, String ticker, OptionType optionType, String strike, String date) {
        OptionPosition optionPosition = (OptionPosition) portfolioPosition;
        assertThat(optionPosition.getPosition()).isEqualTo(new BigDecimal(position));
        assertThat(optionPosition.getTicker()).isEqualTo(ticker);
        assertThat(optionPosition.getOptionType()).isEqualTo(optionType);
        assertThat(optionPosition.getStrike()).isEqualTo(new BigDecimal(strike));
        assertThat(DateUtil.dateFormatter.format(optionPosition.getExpiration())).isEqualTo(date);
    }

    @Test
    public void Stock_Position_Parsed_Correctly() {
        String input =
                "symbol,positionSize\n" +
                        "AAPL,1000";

        List<PortfolioPosition> positions = readPositions(input);
        assertThat(positions.get(0).getPosition()).isEqualTo("1000");
        assertThat(positions.get(0).getTicker()).isEqualTo("AAPL");
    }

    @Test
    public void Call_Option_Parsed_Correctly() {
        String input =
                "symbol,positionSize\n" +
                        "TELSA-NOV-2022-400-C,10000";

        List<PortfolioPosition> positions = readPositions(input);
        assertOptionPosition(positions.get(0),
                10000L,
                "TELSA",
                OptionType.CALL,
                "400",
                "Nov-2022");
    }

    @Test
    public void Put_Option_Parsed_Correctly() {
        String input =
                "symbol,positionSize\n" +
                        "AAPL-OCT-2022-110-P,20000";

        List<PortfolioPosition> positions = readPositions(input);
        assertOptionPosition(positions.get(0),
                20000L,
                "AAPL",
                OptionType.PUT,
                "110",
                "Oct-2022");
    }

    @Test
    public void Multiple_Lines_Of_Input_Parsed_Correctly() {
        String input =
                "symbol,positionSize\n" +
                        "AAPL,1000 \n" +
                        "AAPL-OCT-2022-110-P,20000";

        List<PortfolioPosition> positions = readPositions(input);

        assertThat(positions.get(0).getPosition()).isEqualTo("1000");
        assertThat(positions.get(0).getTicker()).isEqualTo("AAPL");

        assertOptionPosition(positions.get(1),
                20000L,
                "AAPL",
                OptionType.PUT,
                "110",
                "Oct-2022");
    }

    @Test
    public void Empty_Input_Returns_Empty_Portfolio_Position_List() {
        System.setIn(new ByteArrayInputStream(new byte[] {}));
        List<PortfolioPosition> portfolioPositions = positionInputReader.readPositions();
        assertThat(portfolioPositions).isEmpty();
    }

    private List<PortfolioPosition> readPositions(String input) {
        ByteArrayInputStream inContent = new ByteArrayInputStream(input.getBytes());
        System.setIn(inContent);
        return positionInputReader.readPositions();
    }

}