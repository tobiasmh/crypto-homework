package crypto.homework.cryptohomework.subscriber;

import crypto.homework.cryptohomework.model.valuation.PortfolioValuationResult;
import crypto.homework.cryptohomework.model.valuation.PositionValuation;
import org.junit.jupiter.api.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.Arrays;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class PortfolioPrinterTest {

    private PortfolioPrinter portfolioPrinter = new PortfolioPrinter();

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @BeforeEach
    public void before() {
        System.setOut(new PrintStream(outContent));
    }

    @AfterEach
    public void after() {
        System.setOut(originalOut);
    }

    @Test
    public void Portfolio_Output_Is_Correct() {

        PositionValuation stockAppleValuation = new PositionValuation("AAPL", new BigDecimal("109.00"), new BigDecimal("1000"), new BigDecimal("109000.00"), true);
        PositionValuation optionValuation = new PositionValuation("AAPL-OCT-2020-110-C", new BigDecimal("5.50"), new BigDecimal("-20000"), new BigDecimal("-110000.00"), true);
        PositionValuation stockTeslaValuation = new PositionValuation("TESLA", new BigDecimal("110.00"), new BigDecimal("2000"), new BigDecimal("220000.00"), false);

        PortfolioValuationResult valuationResult = new PortfolioValuationResult(Arrays.asList(stockAppleValuation, optionValuation, stockTeslaValuation));
        portfolioPrinter.printValuation(valuationResult);

        String expectedOutput = "## 1 Market Data Update" +
                "\n" +
                "AAPL change to 109.00" +
                "\n" +
                "AAPL-OCT-2020-110-C change to 5.50" +
                "\n" +
                "\n" +
                "## Portfolio\n" +
                String.format("%-20s%20s%20s%20s%n", "symbol", "price", "qty", "value") +
                String.format("%-20s%20s%20s%20s%n", "AAPL", "109.00", "1,000.00", "109,000.00") +
                String.format("%-20s%20s%20s%20s%n", "AAPL-OCT-2020-110-C", "5.50", "-20,000.00", "-110,000.00") +
                String.format("%-20s%20s%20s%20s%n", "TESLA", "110.00", "2,000.00", "220,000.00") +
                "\n" +
                String.format("Total portfolio %64s%n", "219,000.00") +
                "\n" +
                "\n";
        assertThat(outContent.toString()).isEqualTo(expectedOutput);
    }

    @Test
    public void When_No_Prices_Are_Updated_The_Result_Is_Not_Printed() {
        PositionValuation stockAppleValuation = new PositionValuation("AAPL", new BigDecimal("109.00"), new BigDecimal("1000"), new BigDecimal("109000.00"), false);
        PositionValuation optionValuation = new PositionValuation("AAPL-OCT-2020-110-C", new BigDecimal("5.50"), new BigDecimal("-20000"), new BigDecimal("-110000.00"), false);
        PositionValuation stockTeslaValuation = new PositionValuation("TESLA", new BigDecimal("110.00"), new BigDecimal("2000"), new BigDecimal("220000.00"), false);

        PortfolioValuationResult valuationResult = new PortfolioValuationResult(Arrays.asList(stockAppleValuation, optionValuation, stockTeslaValuation));
        portfolioPrinter.printValuation(valuationResult);
        assertThat(outContent.toString()).isEqualTo("");
    }


}