package crypto.homework.cryptohomework.subscriber;

import crypto.homework.cryptohomework.model.instrument.OptionType;
import crypto.homework.cryptohomework.model.position.OptionPosition;
import crypto.homework.cryptohomework.model.position.PortfolioPosition;
import crypto.homework.cryptohomework.model.position.StockPosition;
import crypto.homework.cryptohomework.util.DateUtil;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

@Component
public class PositionInputReader {

    public List<PortfolioPosition> readPositions() {
        Scanner in = new Scanner(System.in);
        List<String> lineInput = new LinkedList<>();
        while (in.hasNext()) {
            lineInput.add(in.nextLine());
        }
        in.close();
        if (lineInput.isEmpty()) {
            return new LinkedList<>();
        }

        lineInput.remove(0);

        List<PortfolioPosition> optionPositions = new LinkedList<>();
        for (String line : lineInput) {
            String[] tokens = line.split(",");

            String[] optionTokens = tokens[0].split("-");

            if (optionTokens.length > 1) {
                optionPositions.add(new OptionPosition(optionTokens[0],
                        new BigDecimal(tokens[1]),
                        new BigDecimal(optionTokens[3]),
                        LocalDate.parse(optionTokens[1] + "-" + optionTokens[2], DateUtil.dateFormatter),
                        parseOptionType(optionTokens[4])
                ));
            } else {
                optionPositions.add(new StockPosition (tokens[0], new BigDecimal(tokens[1].trim())));
            }
        }
        return optionPositions;
    }

    private OptionType parseOptionType(String input) {
        if ("C".equals(input)) {
            return OptionType.CALL;
        } else if ("P".equals(input)) {
            return OptionType.PUT;
        }
        throw new RuntimeException("Unknown Option type");
    }
}
