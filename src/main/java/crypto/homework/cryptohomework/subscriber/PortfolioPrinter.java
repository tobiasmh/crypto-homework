package crypto.homework.cryptohomework.subscriber;

import crypto.homework.cryptohomework.model.valuation.PortfolioValuationResult;
import crypto.homework.cryptohomework.model.valuation.PositionValuation;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class PortfolioPrinter {

    private long updateNumber = 0;

    private final DecimalFormat formatter = new DecimalFormat("#,###.00");

    public void printValuation(PortfolioValuationResult portfolioValuationResult) {
        List<PositionValuation> updatedPositions = portfolioValuationResult.getPositionValuations()
                .stream()
                .filter(PositionValuation::getUpdated)
                .collect(Collectors.toList());
        if (updatedPositions.isEmpty()) {
            return;
        }

        updateNumber++;
        List<PositionValuation> valuations = portfolioValuationResult.getPositionValuations();

        System.out.printf("## %s Market Data Update%n", updateNumber);
        updatedPositions.forEach(valuation -> {
            System.out.printf("%s change to %s%n", valuation.getSymbol(), formatter.format(valuation.getPrice()));
        });
        System.out.println();
        System.out.println("## Portfolio");
        String instrumentFormat = "%-20s%20s%20s%20s%n";
        System.out.printf(instrumentFormat, "symbol", "price", "qty", "value");
        valuations.forEach(valuation -> {
            System.out.printf(instrumentFormat,
                    valuation.getSymbol(),
                    formatter.format(valuation.getPrice()),
                    formatter.format(valuation.getPosition()),
                    formatter.format(valuation.getPositionValue())
            );
        });
        System.out.println();
        System.out.printf("Total portfolio %64s%n",
                formatter.format(portfolioValuationResult.getNetAssetValue()));
        System.out.println();
        System.out.println();
    }

}
