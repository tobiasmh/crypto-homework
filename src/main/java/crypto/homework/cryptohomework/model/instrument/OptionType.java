package crypto.homework.cryptohomework.model.instrument;

public enum OptionType {
    CALL("C"),
    PUT("P");

    final private String inputRepresentation;
    public String getInputRepresentation() {
        return inputRepresentation;
    }

    OptionType(String inputRepresentation) {
        this.inputRepresentation = inputRepresentation;
    }
}
