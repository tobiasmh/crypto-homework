package crypto.homework.cryptohomework.model.instrument;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;

@Entity
public class Stock {

    @Id
    private String ticker;

    public String getTicker() {
        return ticker;
    }

    private BigDecimal price;
    public BigDecimal getPrice() {
        return price;
    }
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    private BigDecimal standardDeviation;
    public BigDecimal getStandardDeviation() {
        return standardDeviation;
    }

    private BigDecimal expectedReturn;
    public BigDecimal getExpectedReturn() {
        return expectedReturn;
    }

    public Stock() {

    }

    public Stock(String ticker, BigDecimal price, BigDecimal standardDeviation, BigDecimal expectedReturn) {
        this.ticker = ticker;
        this.price = price;
        this.standardDeviation = standardDeviation;
        this.expectedReturn = expectedReturn;
    }
}
