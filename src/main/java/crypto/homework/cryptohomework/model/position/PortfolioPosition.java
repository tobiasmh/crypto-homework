package crypto.homework.cryptohomework.model.position;

import java.math.BigDecimal;

public abstract class PortfolioPosition {

    final private String ticker;

    final private BigDecimal position;

    public PortfolioPosition(String ticker, BigDecimal position) {
        this.ticker = ticker;
        this.position = position;
    }

    public String getTicker() {
        return ticker;
    }

    public BigDecimal getPosition() {
        return position;
    }

    abstract public String getSymbol();
}
