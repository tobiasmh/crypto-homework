package crypto.homework.cryptohomework.model.position;

import crypto.homework.cryptohomework.model.instrument.OptionType;
import crypto.homework.cryptohomework.util.DateUtil;

import java.math.BigDecimal;
import java.time.LocalDate;

public class OptionPosition extends PortfolioPosition {

    final private BigDecimal strike;

    final private LocalDate expiration;

    final private OptionType optionType;

    public OptionPosition(String ticker, BigDecimal position, BigDecimal strike, LocalDate expiration, OptionType optionType) {
        super(ticker, position);
        this.strike = strike;
        this.expiration = expiration;
        this.optionType = optionType;
    }

    public BigDecimal getStrike() {
        return strike;
    }

    public LocalDate getExpiration() {
        return expiration;
    }

    public OptionType getOptionType() {
        return optionType;
    }

    @Override
    public String getSymbol() {
        return String.format("%s-%s-%s-%s", this.getTicker(), DateUtil.dateFormatter.format(this.expiration), this.strike, this.optionType.getInputRepresentation());
    }
}
