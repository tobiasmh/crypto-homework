package crypto.homework.cryptohomework.model.position;

import java.math.BigDecimal;

public class StockPosition extends PortfolioPosition {
    public StockPosition(String ticker, BigDecimal position) {
        super(ticker, position);
    }

    @Override
    public String getSymbol() {
        return this.getTicker();
    }
}
