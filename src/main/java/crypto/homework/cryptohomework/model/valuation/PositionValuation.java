package crypto.homework.cryptohomework.model.valuation;

import java.math.BigDecimal;

public class PositionValuation {

    final private String symbol;
    public String getSymbol() {
        return symbol;
    }

    final private BigDecimal price;
    public BigDecimal getPrice() {
        return price;
    }

    final private BigDecimal position;
    public BigDecimal getPosition() {
        return position;
    }

    final private BigDecimal positionValue;
    public BigDecimal getPositionValue() {
        return positionValue;
    }

    final private Boolean updated;
    public Boolean getUpdated() {
        return updated;
    }

    public PositionValuation(String symbol, BigDecimal price, BigDecimal position, BigDecimal positionValue, Boolean updated) {
        this.symbol = symbol;
        this.price = price;
        this.position = position;
        this.positionValue = positionValue;
        this.updated = updated;
    }
}
