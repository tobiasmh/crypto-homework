package crypto.homework.cryptohomework.model.valuation;

import java.math.BigDecimal;
import java.util.List;

public class PortfolioValuationResult {

    final private List<PositionValuation> positionValuations;

    final private BigDecimal netAssetValue;

    public PortfolioValuationResult(List<PositionValuation> positionValuations) {
        this.positionValuations = positionValuations;
        this.netAssetValue = positionValuations.stream()
                .map(PositionValuation::getPositionValue)
                .reduce(new BigDecimal(0), (position, accumulator) -> accumulator.add(position));;
    }

    public List<PositionValuation> getPositionValuations() {
        return positionValuations;
    }

    public BigDecimal getNetAssetValue() {
        return netAssetValue;
    }
}
