package crypto.homework.cryptohomework.publisher;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.springframework.stereotype.Component;

@Component
public class BlackScholesPricer {

    private final NormalDistribution normalDistribution = new NormalDistribution();

    public double callOption(double stockPrice,
                                 double strikePrice,
                                 double riskFreeRate,
                                 double yearsUntilMaturity,
                                 double stockStandardDeviation
                                 ) {
        double d1 = d1(stockPrice, strikePrice, riskFreeRate, yearsUntilMaturity, stockStandardDeviation);
        double d2 = d2(d1, stockStandardDeviation, yearsUntilMaturity);

        return stockPrice * normalDistribution.cumulativeProbability(d1) - strikePrice * Math.exp(-( riskFreeRate * yearsUntilMaturity)) * normalDistribution.cumulativeProbability(d2);
    }

    public double putOption(double stockPrice,
                                    double strikePrice,
                                    double riskFreeRate,
                                    double yearsUntilMaturity,
                                    double stockStandardDeviation) {
        double d1 = d1(stockPrice, strikePrice, riskFreeRate, yearsUntilMaturity, stockStandardDeviation);
        double d2 = d2(d1, stockStandardDeviation, yearsUntilMaturity);

        return strikePrice * Math.exp(-(riskFreeRate * yearsUntilMaturity)) * normalDistribution.cumulativeProbability(-d2) - stockPrice * normalDistribution.cumulativeProbability(-d1);
    }


    public static double d2(double d1, double stockStandardDeviation, double yearsUntilMaturity) {
        return d1 - stockStandardDeviation * Math.sqrt(yearsUntilMaturity);
    }

    public double d1(double stockPrice, double strikePrice,
                          double riskFreeRate,
                          double yearsUntilMaturity,
                          double stockStandardDeviation) {
        double logOfStockAndStrikePrice = Math.log(stockPrice / strikePrice);
        double riskFreeRateAndStandardDeviation = (riskFreeRate) + (Math.pow(stockStandardDeviation, 2) / 2);
        double top = (logOfStockAndStrikePrice + riskFreeRateAndStandardDeviation * yearsUntilMaturity);
        double bottom = stockStandardDeviation * Math.sqrt(yearsUntilMaturity);
        return top / bottom;
    }

}
