package crypto.homework.cryptohomework.publisher;

import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class BrownianMotionDiscreteTimeGenerator {

    private final Random random = new Random();

    public double generateReturn(double expectedReturn, double numberOfSeconds, double standardDeviation) {
        return expectedReturn * (numberOfSeconds / 7_257_600) + standardDeviation * random.nextGaussian() * Math.sqrt(numberOfSeconds / 7_257_600);
    }

}
