package crypto.homework.cryptohomework.publisher;

import crypto.homework.cryptohomework.StockRepository;
import crypto.homework.cryptohomework.model.instrument.OptionType;
import crypto.homework.cryptohomework.model.instrument.Stock;
import crypto.homework.cryptohomework.model.position.OptionPosition;
import crypto.homework.cryptohomework.model.position.PortfolioPosition;
import crypto.homework.cryptohomework.model.position.StockPosition;
import crypto.homework.cryptohomework.model.valuation.PortfolioValuationResult;
import crypto.homework.cryptohomework.model.valuation.PositionValuation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.MathContext;
import java.time.Clock;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
public class PricePublisher {

    private static final double RISK_FREE_RATE = 0.02;

    private static final Logger logger = LoggerFactory.getLogger(PricePublisher.class);

    private final StockRepository stockRepository;
    private final BrownianMotionDiscreteTimeGenerator brownianMotionDiscreteTimeGenerator;
    private final BlackScholesPricer blackScholesPricer;
    private final Clock clock;
    private final Random random;

    private final List<PriceUpdateCallback> priceUpdateCallbacks = new LinkedList<>();
    public void subscribe(PriceUpdateCallback priceUpdateCallback) {
        priceUpdateCallbacks.add(priceUpdateCallback);
    }

    private Boolean pricePublishingEnabled = true;

    private Thread pricePublisherThread;
    public void interruptPricePublisherThread() {
        pricePublisherThread.interrupt();
    }

    public PricePublisher(StockRepository stockRepository,
                          BrownianMotionDiscreteTimeGenerator brownianMotionDiscreteTimeGenerator, BlackScholesPricer blackScholesPricer, Clock clock, Random random) {
        this.stockRepository = stockRepository;
        this.brownianMotionDiscreteTimeGenerator = brownianMotionDiscreteTimeGenerator;
        this.blackScholesPricer = blackScholesPricer;
        this.clock = clock;
        this.random = random;

        startPriceUpdaterThread(stockRepository);
    }

    private void startPriceUpdaterThread(StockRepository stockRepository) {
        Runnable runnable = () -> {
            Iterable<Stock> allStocks = stockRepository.findAll();
            Map<String, Stock> stocksByTicker = StreamSupport
                    .stream(allStocks.spliterator(), false)
                    .collect(Collectors.toMap(Stock::getTicker, (stock) -> stock));

            while (pricePublishingEnabled) {
                try {
                    double numberOfSecondsBetweenUpdates = ThreadLocalRandom.current().nextDouble(0.5, 2.0);

                    List<Stock> updatedStocks = updateStockPrices(allStocks, numberOfSecondsBetweenUpdates);

                    priceUpdateCallbacks.forEach(callback -> {
                        List<PositionValuation> positionValuations = positionValuations(stocksByTicker,
                                callback.portfolioPositions(),
                                updatedStocks);
                        callback.priceIsUpdated(new PortfolioValuationResult(positionValuations));
                    });

                    Thread.sleep((long)(numberOfSecondsBetweenUpdates * 1000));
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        pricePublisherThread = new Thread(runnable);
        pricePublisherThread.start();
    }

    private List<PositionValuation> positionValuations(Map<String, Stock> stocksByTicker,
                                                       List<PortfolioPosition> portfolioPositions,
                                                       List<Stock> updatedStocks) {

        return portfolioPositions.stream().map(position -> {
            Stock stock = stocksByTicker.get(position.getTicker());
            boolean isUpdated = updatedStocks.contains(stock);

            if (position instanceof StockPosition) {
                BigDecimal positionValue = stock.getPrice().multiply(position.getPosition());
                return new PositionValuation(stock.getTicker(), stock.getPrice(), position.getPosition(), positionValue, isUpdated);

            } else if (position instanceof OptionPosition) {
                OptionPosition optionPosition = (OptionPosition) position;

                double yearsUntilMaturity = LocalDate.now(clock).until(optionPosition.getExpiration(), ChronoUnit.DAYS) / 365.2425f;
                BigDecimal price = new BigDecimal(priceOption(stock, optionPosition, yearsUntilMaturity));

                BigDecimal positionValue = price.multiply(position.getPosition());
                return new PositionValuation(position.getSymbol(), price, position.getPosition(), positionValue, isUpdated);

            } else {
                logger.warn("Unknown position type {}", position);
                return null;
            }
        }).collect(Collectors.toList());
    }

    private double priceOption(Stock stock, OptionPosition optionPosition, double yearsUntilMaturity) {
        if (optionPosition.getOptionType() == OptionType.CALL) {
            return blackScholesPricer.callOption(stock.getPrice().doubleValue(),
                    optionPosition.getStrike().doubleValue(),
                    RISK_FREE_RATE,
                    yearsUntilMaturity,
                    stock.getStandardDeviation().doubleValue());

        } else if (optionPosition.getOptionType() == OptionType.PUT) {
            return blackScholesPricer.putOption(stock.getPrice().doubleValue(),
                    optionPosition.getStrike().doubleValue(),
                    RISK_FREE_RATE,
                    yearsUntilMaturity,
                    stock.getStandardDeviation().doubleValue());

        } else {
            logger.warn("Unknown option type {}", optionPosition.getOptionType());
        }
        return 0;
    }

    private List<Stock> updateStockPrices(Iterable<Stock> allStocks, double numberOfSeconds) {

        return StreamSupport.stream(allStocks.spliterator(), false)
                .filter(it -> random.nextBoolean())
                .peek(stock -> {
                    double randomReturn = brownianMotionDiscreteTimeGenerator.generateReturn(
                            stock.getExpectedReturn().doubleValue(),
                            numberOfSeconds,
                            stock.getStandardDeviation().doubleValue());
                    BigDecimal priceMovement = stock.getPrice().multiply(BigDecimal.valueOf(randomReturn));
                    BigDecimal updatedPrice = stock.getPrice().add(priceMovement);

                    stock.setPrice(updatedPrice.round(new MathContext(4)));
                    stockRepository.save(stock);
                }).collect(Collectors.toList());
    }

    public interface PriceUpdateCallback {
        void priceIsUpdated(PortfolioValuationResult portfolioValuationResult);
        List<PortfolioPosition> portfolioPositions();
    }
}
