package crypto.homework.cryptohomework;

import crypto.homework.cryptohomework.model.instrument.Stock;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StockRepository extends CrudRepository<Stock, String> {
}
