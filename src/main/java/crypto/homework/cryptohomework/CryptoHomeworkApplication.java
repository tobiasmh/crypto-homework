package crypto.homework.cryptohomework;

import crypto.homework.cryptohomework.model.position.PortfolioPosition;
import crypto.homework.cryptohomework.model.valuation.PortfolioValuationResult;
import crypto.homework.cryptohomework.publisher.PricePublisher;
import crypto.homework.cryptohomework.subscriber.PortfolioPrinter;
import crypto.homework.cryptohomework.subscriber.PositionInputReader;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
public class CryptoHomeworkApplication implements CommandLineRunner {

    private final PricePublisher pricePublisher;

    private final PortfolioPrinter portfolioPrinter;

    private final PositionInputReader positionInputReader;

    public CryptoHomeworkApplication(PricePublisher pricePublisher, PortfolioPrinter portfolioPrinter, PositionInputReader positionInputReader) {
        this.pricePublisher = pricePublisher;
        this.portfolioPrinter = portfolioPrinter;
        this.positionInputReader = positionInputReader;
    }

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(CryptoHomeworkApplication.class);
        app.run(args);
    }

    @Override
    public void run(String... args) throws Exception {

        List<PortfolioPosition> portfolioPositions = positionInputReader.readPositions();

        pricePublisher.subscribe(new PricePublisher.PriceUpdateCallback() {

            @Override
            public void priceIsUpdated(PortfolioValuationResult portfolioValuation) {
                portfolioPrinter.printValuation(portfolioValuation);
            }

            @Override
            public List<PortfolioPosition> portfolioPositions() {
                return portfolioPositions;
            }
        });

        while (true) {
            Thread.sleep(10);
        }

    }

}
